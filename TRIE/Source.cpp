#include<iostream>
template <class T>
class SimpleVector
{
public:
	SimpleVector()
	{
		size = 2; //domyslnie nowy wektor na 2
		elements = 0;
		arr = new T[size];
		for (int i = 0; i < size; ++i) //inicjalizacja  TODO: ogarnac rozne w zaleznosci od typu
		{
			arr[i] = nullptr;
		}
	}
	~SimpleVector()
	{
		delete[] arr;
	}

private:
	T *arr; //wskaznik na pierwszy element
	int size;
	int elements;

public:
	int CurrentElements()const
	{
		return elements;
	}
	T& operator[](int i)
	{
		return arr[i];
	}
	const T& operator[](int i) const //overload operatora -- aby latwiej zwracac element -- operator musi zwracac referencje!
	{
		return arr[i];
	}
	void Pushback(const T& value) //dodawanie "na koniec"
	{
		CheckAndIncreaseCapacity();
		arr[elements] = value;
		++elements;
	}
	void Pop()
	{
		if (elements > 0)
		{
			--elements;
		}
	}
	void Insert(const T& value, int index) //dodanie do dowolnego "aktywnego" indeksu
	{
		CheckAndIncreaseCapacity();
		if (elements > 0 && elements > index)
		{
			for (int j = elements; j > index; --j)
			{
				arr[j] = arr[j - 1];
			}
		}
		++elements;
		arr[index] = value;
	}

private:
	void CheckAndIncreaseCapacity()
	{
		if (elements + 1 <= size) return;
		size *= 2;//nowy dwa razy wiekszy
		if (size > 27)
			size = 27;
		T *arr2 = new T[size];
		for (int i = 0; i < elements; i++)
		{
			arr2[i] = arr[i];
		}
		delete[] arr;
		arr = arr2;
	}
};

struct trieNode
{
	char* translation; //jezeli nie null to posiada tlumaczenie
	char letter;   //Etykieta
	SimpleVector<trieNode*>children;

	trieNode() //konstruktor do incijalizacji - inaczej na stosie nie dziala
	{
		translation = nullptr;
		letter = '\0';
	}
	~trieNode()
	{
		if (translation != nullptr)
			delete[] translation;
		for (int i = 0; i < children.CurrentElements(); i++)
		{
			delete children[i];
		}
	}
};

int WordLength(const char *c) //liczy dlugosc slowa
{
	int i = 0;
	while (c[i] != '\0')
	{
		++i;
	}
	return i;
}

void AddTranslation(trieNode &node, const char *translation) //Funkcja dodajaca tlumaczenie do danego node'a
{
	node.translation = new char[WordLength(translation) + 1]; //+1 bo na \0
	for (int i = 0; i < WordLength(translation) + 1; i++)
	{
		node.translation[i] = translation[i];
	}
}

trieNode *AddAndSortChild(trieNode &node, char c) //funkcja dodajaca nowe dziecko i ustawiajaca indeks by w vectorze etykiety wskaznikow byly alfabatycznie
{
	int i;
	trieNode *newChild = new trieNode;
	newChild->letter = c;

	for (i = 0; i < node.children.CurrentElements(); i++)
	{
		if (node.children[i]->letter > c)
			break;
	}
	node.children.Insert(newChild, i);
	return newChild;
}

trieNode *BinarySearchEtiquette(char c, trieNode &node) //funkcja wyszukajaca binarnie node z dana etykieta i zwracajaca go, jezeli brak to nullptr
{
	int low = 0, high = node.children.CurrentElements() - 1, midpoint = 0;
	while (low <= high)
	{
		midpoint = low + (high - low) / 2;
		if (c == node.children[midpoint]->letter)
		{
			return node.children[midpoint];
		}
		else if (c < node.children[midpoint]->letter)
			high = midpoint - 1;
		else
			low = midpoint + 1; //kiedy c > od litery w srodku tablicy
	}
	return nullptr;
}

void AddWord(const char *word, const char *translation, trieNode &root) //funkcja dodajaca slowo wraz z tlumaczeniem
{
	trieNode *helper = &root; // wskazniki pomocnicze - do poruszania
	trieNode *helper2;
	for (int i = 0; i < WordLength(word); i++) //sprawdzamy przez wszystkie litery dodawanego slowa
	{
		helper2 = BinarySearchEtiquette(word[i], *helper);
		if (helper2 == nullptr) //jezeli brak to dodaj
		{
			helper = AddAndSortChild(*helper, word[i]);
		}
		else //przechodzimy na kolejne dziecko
		{
			helper = helper2;
		}
	}
	AddTranslation(*helper, translation);
}

void FindWordTranslation(const char *word, trieNode &root)//Funkcja szukajaca tlumaczenia konkretnego slowa
{
	trieNode *helper = &root;
	trieNode *helper2;
	for (int i = 0; i < WordLength(word); ++i)
	{
		helper2 = BinarySearchEtiquette(word[i], *helper);
		if (helper2 == nullptr) //jezeli brak to wypisz
		{
			std::cout << "-" << std::endl;
			return;
		}
		else
		{
			helper = helper2;
		}
	}
	if (helper->translation != nullptr) //jezeli istnieje
	{
		std::cout << helper->translation << std::endl;
	}
	else
	{
		std::cout << "-" << std::endl;
	}
}

void printAllTranslations(const trieNode &node, bool *translationPrinted) //funkcja rekruencyjnie wypisujaca tlumaczenia wszystkich dzieci z danego wezla
{
	if (node.translation != nullptr)
	{
		std::cout << node.translation << std::endl;
		*translationPrinted = true;
	}

	for (int i = 0; i < node.children.CurrentElements(); i++) //przechodzenie przez kolejne dzieci, DFS
	{
		printAllTranslations(*(node.children[i]), translationPrinted);
	}
}

void FindWordsWithPrefix(const char *prefix, trieNode &root)
{
	trieNode *helper = &root;
	trieNode *helper2;
	for (int i = 0; i < WordLength(prefix); ++i)
	{
		helper2 = BinarySearchEtiquette(prefix[i], *helper);
		if (helper2 == nullptr)
		{
			std::cout << "-" << std::endl;
			return;
		}
		else
		{
			helper = helper2;
		}
	}
	//helper zawiera node od kt�rego wypisujemy wszystkie t�umaczenia
	bool translationPrinted = false; //zeby zapamieta� je�eli jakies t�umaczenie zostanie wypisane
	printAllTranslations(*helper, &translationPrinted);
	if (translationPrinted == false)
		std::cout << "-" << std::endl;	//nie by�o �adnego s�owa z danym prefixem
}

int main()
{
	trieNode root;
	char word[17], command, translation[17];
	while (std::cin >> command)
	{
		std::cin >> std::ws;

		switch (command)
		{
		case'+':
			std::cin >> word;
			std::cin >> std::ws;
			std::cin >> translation;
			AddWord(word, translation, root);
			break;
		case '?':
			std::cin >> word;
			FindWordTranslation(word, root);
			break;
		case '*':
			std::cin >> word;
			FindWordsWithPrefix(word, root);
			break;
		default:
			break;
		}
	}

	return 0;
}