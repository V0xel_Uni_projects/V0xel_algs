#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <cmath>
/*
Please note that it's Function problem i.e.
you need to write your solution in the form of Function(s) only.
Driver Code to call/invoke your function would be added by GfG's Online Judge.*/

struct Node
{
	int data;
	Node* next;
};
/* Should return data of middle node. If linked list is empty, then  -1*/
int getMiddle(Node *head)
{
	if (head == nullptr) { return -1; }
	Node *tmp = head;
	double count = 0;
	do
	{
		count++;
		tmp = tmp->next;
	} while (tmp != nullptr);
	double finalCount = ceil(count / 2);
	int data;
	Node *tmp2 = head;
	for (int i = 0; i < finalCount; i++)
	{
		data = tmp2->data;
		tmp2 = tmp2->next;
	}
	return data;
}

int main()
{
	std::cin.get();
}