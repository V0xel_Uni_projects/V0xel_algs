#include<iostream>

struct List
{
	int var;
	List *next;
	void Print()
	{
		std::cout << var << " ";
	}
};

void AddToList(List **head, int arg) //wskaznik na wskaznik bo chcemy przekazac wraz z adresem --tak jakby przez "referencje"
{
	List *newitem = new List; // utworzenie nowego elementu listy poprzez wskaznik
	newitem->var = arg; //przypisanie wartosci nowemu
	newitem->next = *head; // nastepnik newitem wskazuje na to co glowa
	*head = newitem; //wskaznik na glowe pokazuje na newitem
}

void PrintList(List *root)
{
	if (root == nullptr)
	{
		return;
	}
	while (root != nullptr) //przejscie po calej liscie - dopoki wskaznik na nastepnik nie wskazuje na nic
	{
		root->Print();
		root = root->next; //przejscie na element na ktory wskazuje
	}
}

void SwapOnList(List **head)
{
	List *helper1;
	helper1 = *head;
	if ((*head)->next == nullptr)
	{
		return;
	}
	*head = (*head)->next;
	helper1->next = (*head)->next;
	(*head)->next = helper1;
}

void DeleteHead(List **head)
{
	if (*head == nullptr) //jezeli glowa wskazuje na nic to lista pusta wiec nie ma co usunac
	{
		return;
	}

	List *helper;
	helper = (*head);
	*head = (*head)->next;
	delete helper;
}

void Sum(List **head)
{
	List *elem2 = (*head)->next;
	elem2->var = (elem2->var) + ((*head)->var);
	DeleteHead(head);
}

void Magic(List **head, int n)
{
	if (*head == nullptr || n == 1 || n == 0) //jezeli stos pusty lub n=1 lub n=0
	{
		return;
	}
	/*------To samo co w usuwaniu ale bez usuwania heleper'a----*/
	List *helper, *helper2;
	helper2 = (*head);
	helper = (*head);
	if ((*head)->next == nullptr) //TODO: przeniesc to do pierwszego warunku
	{
		return;
	}
	for (int i = 0; i <= n - 2; i++) //n-2 bo przesuniec o dwa - jestesmy juz na pierwszym a szukamy poprzednika tego gdzie bedziemy
	{
		helper2 = helper2->next;
	}
	if (helper2 == nullptr) //jezeli przesunelismy sie az do pustego -- chcielismy przejsc dalej niz jest elementow
	{
		return;
	}
	*head = (*head)->next; //zmieniamy glowe na kolejny element - bo pierwszy bedzie zmieniany
	helper->next = helper2->next;
	helper2->next = helper;
}

int main()
{
	int n = 0;
	char command;
	List *head = nullptr; //glowa listy - wskaznik na glowe - pierwszy element

	while (true)
	{
		std::cin >> std::ws;   //wyrzucenie bialych znakow z wejscia
		if (('a' <= std::cin.peek() && std::cin.peek() <= 'z') || std::cin.peek() == '+') //sprawdzamy czy kolejny jest char
		{
			if ('q' == std::cin.peek()) //jezeli q to wyjdz z petli - zakoncz program
			{
				std::cin >> command;
				break;
			}

			std::cin >> command;

			switch (command) //reszta komend
			{
			case 'p':
				PrintList(head);
				break;
			case 's':
				SwapOnList(&head);
				break;
			case 'x':
				DeleteHead(&head);
				break;
			case '+':
				Sum(&head);
				break;
			case'r':
				DeleteHead(&head); //musimy usunac ostatni dodany na stos, poniewaz jest on wtedy czescia komendy
				Magic(&head, n);
				break;

			default:
				break;
			}
		}
		else //jezeli liczba
		{
			std::cin >> n;
			AddToList(&head, n);
		}
	}

	return 0;
}