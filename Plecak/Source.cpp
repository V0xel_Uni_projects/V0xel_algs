#include<iostream>
template <class T>
class SimpleVector
{
public:
	SimpleVector(int s) : size(s), elements(s)
	{
		arr = new T[size];
	}
	SimpleVector(int s, const T& value) : size(s), elements(s)
	{
		arr = new T[size];
		for (int i = 0; i < elements; i++)
		{
			arr[i] = value;
		}
	}
	SimpleVector() : size(2), elements(0)
	{
		arr = new T[size];
	}
private:
	T *arr;
	int size;
	int elements;

public:
	int CurrentElements()const
	{
		return elements;
	}
	T& operator[](int i)
	{
		return arr[i];
	}
	const T& operator[](int i) const
	{
		return arr[i];
	}
	void Pushback(const T& value)
	{
		CheckAndIncreaseCapacity();
		arr[elements] = value;
		++elements;
	}
	void Pop()
	{
		if (elements > 0)
		{
			--elements;
		}
	}
	void Insert(const T& value, int index)
	{
		CheckAndIncreaseCapacity();
		if (elements > 0 && elements > index)
		{
			for (int j = elements; j > index; --j)
			{
				arr[j] = arr[j - 1];
			}
		}
		++elements;
		arr[index] = value;
	}

private:
	void CheckAndIncreaseCapacity()
	{
		if (elements + 1 <= size) return;
		size *= 2;
		T*arr2 = new T[size];
		for (int i = 0; i < elements; i++)
		{
			arr2[i] = arr[i];
		}
		delete[] arr;
		arr = arr2;
	}
};

struct Object
{
	int weight;
	int value;
	int ID;
};

class Backpack
{
public:
	Backpack(int numOfElements, int volume) : interior(numOfElements), calculation(numOfElements + 1, SimpleVector<int>(volume + 1)), totalVolume(volume), totalElements(numOfElements) {}
private:
	int totalVolume;
	int totalElements;
	SimpleVector<Object>interior; //elementy -1 przy indeksach potem w glownej po interior indeksowane od 0
	SimpleVector<SimpleVector<int>>calculation;  //do obliczen
	SimpleVector<int>finalIDs; // vector ostatetecznych elementow w spakowanym
public:
	void Calculate()
	{
		for (int i = 0; i < totalElements; i++)
		{
			calculation[i][0] = 0;
		}
		for (int i = 0; i < totalVolume; i++)
		{
			calculation[0][i] = 0;
		}
		for (int i = 1; i <= totalElements; i++)
		{
			for (int j = 0; j <= totalVolume; j++)
			{
				calculation[i][j] = (interior[i - 1].weight >= j) ? calculation[i - 1][j] : max(calculation[i - 1][j], calculation[i - 1][j - interior[i - 1].weight] + interior[i - 1].value);
			}
		}
	}

	void AddObject(int index, int a, int b)
	{
		interior[index].weight = a;
		interior[index].value = b;
		interior[index].ID = index;
	}

	void PrintSolution()
	{
		std::cout << calculation[totalElements][totalVolume] << "\n";
	}

	void SaveElements()
	{
		int w = totalVolume, n = totalElements;
		while (n > 0)
		{
			if (calculation[n][w] == calculation[n - 1][w - interior[n - 1].weight] + interior[n - 1].value) //przedmiot w plecaku
			{
				finalIDs.Pushback(interior[n - 1].ID + 1); //+1 do ID bo indeksacja w interior od 0
				w = w - interior[n - 1].weight;
				n--;
			}
			else
			{
				n--;
			}
		}
	}

	void PrintFinalElements()
	{
		for (int i = finalIDs.CurrentElements()-1; i >= 0; i--)
		{
			std::cout << finalIDs[i] << " ";
		}
	}

private:
	int max(int a, int b)
	{
		if (a >= b)
		{
			return a;
		}
		else
		{
			return b;
		}
	}
};

int main()
{
	int volume, elements, weight, value;
	std::cin >> volume;
	std::cin >> elements;
	while (true)
	{
		Backpack bag(elements, volume);
		for (int i = elements; i > 0; i--)
		{
			int index = elements - i;
			std::cin >> weight;
			std::cin >> value;
			bag.AddObject(index, weight, value);
		}
		bag.Calculate();
		bag.PrintSolution();
		bag.SaveElements();
		bag.PrintFinalElements();
	}

	return 0;
}