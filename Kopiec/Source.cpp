#include<iostream>
template <class T>
class SimpleVector
{
public:
	SimpleVector()
	{
		size = 2; //domyslnie nowy wektor na 2
		elements = 0; //na poczatku brak elementow-->troche fake gdy inicjalizujemy do 0
		arr = new T[size];
		for (int i = 0; i < size; ++i) //inicjalizacja do 0 -->mozna by se odpuscic
		{
			arr[i] = 0;
		}
	}
	~SimpleVector()
	{
		delete[] arr;
	}

private:
	T *arr; //wskaznik na pierwszy element
	int size;
	int elements;

public:
	int CurrentElements()const
	{
		return elements;
	}
	T& operator[](int i)
	{
		return arr[i];
	}
	const T& operator[](int i) const //overload operatora -- aby latwiej zwracac element -- operator musi zwracac referencje!
	{
		return arr[i];
	}
	void pushback(const T& value) //dodawanie "na koniec"
	{
		CheckAndIncreaseCapacity();
		arr[elements] = value;
		++elements;
	}
	void pop()//"usuwa" ostatni element w vectorze przez zmniejszenie liczby aktywnych elementow
	{
		if (elements > 0)
		{
			--elements;
		}
	}

private:
	void CheckAndIncreaseCapacity()
	{
		if (elements + 1 <= size) return;
		size *= 2;                                 //nowy dwa razy wiekszy
		T *arr2 = new T[size];
		for (int i = 0; i < elements; i++)
		{
			arr2[i] = arr[i];
		}
		delete[] arr;
		arr = arr2;
	}
};

void AddToMaxHeap(SimpleVector<int> &heap, int value)
{
	int parentPos, currentIndex = heap.CurrentElements(); //wielkosc przed dodaniem nastepnego elementu-->indeks aktualnego-->bo pushback zwiekszy o 1
	heap.pushback(value);
	if (currentIndex == 0) return;
	parentPos = (currentIndex - 1) / 2;
	while (currentIndex > 0 && heap[parentPos] < value) //>= ?
	{
		heap[currentIndex] = heap[parentPos]; //ojciec do syna--wartosc ojca do indeksu syna
		currentIndex = parentPos; //przenosimy sie do ojca--zmieniamy indeks
		parentPos = (currentIndex - 1) / 2;;//liczymy nowego ojca
	}
	heap[currentIndex] = value;
}

void AddToMinHeap(SimpleVector<int> &heap, int value) //to samo co w max tylko w warunku >
{
	int parentPos, currentIndex = heap.CurrentElements();
	heap.pushback(value);
	if (currentIndex == 0) return;
	parentPos = (currentIndex - 1) / 2;
	while (currentIndex > 0 && heap[parentPos] > value)
	{
		heap[currentIndex] = heap[parentPos];
		currentIndex = parentPos;
		parentPos = (currentIndex - 1) / 2;;
	}
	heap[currentIndex] = value;
}

void MaxHeapify(SimpleVector<int> &heap, int index) //naprawia kopiec Max, index-->"miejsce rozpoczecia"
{
	while (index <= heap.CurrentElements())
	{
		int left = 2 * index + 1, right = 2 * index + 2, biggest, helper; //indeksy dzieci i zmienne pomocnicze
		if (left < heap.CurrentElements() && heap[left] > heap[index])
		{
			biggest = left;
		}
		else
		{
			biggest = index;
		}
		if (right < heap.CurrentElements() && heap[right] > heap[biggest])
		{
			biggest = right;
		}
		if (biggest != index)
		{
			helper = heap[index];
			heap[index] = heap[biggest];
			heap[biggest] = helper;
			index = biggest;
		}
		else
		{
			break;
		}
	}
}

void MinHeapify(SimpleVector<int> &heap, int index)
{
	while (index <= heap.CurrentElements())
	{
		int left = 2 * index + 1, right = 2 * index + 2, smallest, helper;
		if (left < heap.CurrentElements() && heap[left] < heap[index])
		{
			smallest = left;
		}
		else
		{
			smallest = index;
		}
		if (right < heap.CurrentElements() && heap[right] < heap[smallest]) // <= ??
		{
			smallest = right;
		}
		if (smallest != index)
		{
			helper = heap[index];
			heap[index] = heap[smallest];
			heap[smallest] = helper;
			index = smallest;
		}
		else
		{
			break;
		}
	}
}

void DeleteMaxRoot(SimpleVector<int> &heap)
{
	if (heap.CurrentElements() == 0)return;
	int value, rootValue; //zmienne do indeksow pomocniczych i wartosc ostatniego
	rootValue = heap[0];
	heap[0] = heap[heap.CurrentElements() - 1];//przepisujemy wartosc ostatniego elementu
	heap.pop(); // usuwamy ostatni - pop zmniejsza rowniez rozmiar
	MaxHeapify(heap, 0);//naprawianie od korzenia -- samo Heapify bo "jest zbudowany"
	std::cout << rootValue << "\n";
}

void DeleteMinRoot(SimpleVector<int> &heap) //To samo co w Max tylko z wywolaniem Min
{
	if (heap.CurrentElements() == 0)return;
	int value, rootValue;
	rootValue = heap[0];
	heap[0] = heap[heap.CurrentElements() - 1];
	heap.pop();
	MinHeapify(heap, 0);
	std::cout << rootValue << "\n";
}

void ModifyHeap(SimpleVector<int> &heap, bool check) //modyfikuje z min na max i na odwrot poprzez pelne budowanie kopca
{
	if (heap.CurrentElements() == 0 || heap.CurrentElements() == 1)return;
	if (check)
	{
		for (int i = (heap.CurrentElements() / 2) - 1; i >= 0; i--) // -1 bo indeksuje od 0 --> szukamy ostatniego przed lisciem
		{
			MinHeapify(heap, i);
		}
	}
	else
	{
		for (int i = (heap.CurrentElements() / 2) - 1; i >= 0; i--)
		{
			MaxHeapify(heap, i);
		}
	}
}

void PrintHeap(SimpleVector<int> &heap)
{
	for (int i = 0; i < heap.CurrentElements(); i++)
	{
		std::cout << heap[i] << " ";
	}
}

void RewriteHeap(SimpleVector<int> &heap, int value, bool choice, int var)//usuwa cala tablce, wpisuje elementy i buduje nowa
{
	for (int i = heap.CurrentElements(); i > 0; i--)
	{
		heap.pop();
	}
	for (int i = 0; i < var; i++)
	{
		std::cin >> value;
		heap.pushback(value);
	}
	ModifyHeap(heap, choice); //bo budujemy "od zera"
}

int main()
{
	int value = 0, var = 0;
	char command;
	bool minMax = false;
	SimpleVector<int> heap;

	while (std::cin >> command)
	{
		if (command == 'q')break;

		switch (command)
		{
		case'+':
			std::cin >> value;
			if (minMax) AddToMinHeap(heap, value);
			else AddToMaxHeap(heap, value);
			break;
		case 'p':
			PrintHeap(heap);
			break;
		case '-':
			if (minMax)DeleteMinRoot(heap); else
				DeleteMaxRoot(heap);
			break;
		case 'r':
			std::cin >> var;
			RewriteHeap(heap, value, minMax, var);
			break;
		case 's':
			minMax = !minMax;
			ModifyHeap(heap, minMax);
			break;
		default:
			break;
		}
	}

	return 0;
}