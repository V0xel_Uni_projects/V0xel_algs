#include<iostream>
int CHILDREN_ARRAY_SIZE = 8;
template <class T>
class SimpleVector
{
public:
	SimpleVector()
	{
		size = 1; //domyslnie nowy wektor na 1
		elements = 0;
		arr = new T[size];
		for (int i = 0; i < size; ++i) //inicjalizacja  TODO: ogarnac rozne w zaleznosci od typu
		{
			arr[i] = nullptr;
		}
	}
	~SimpleVector()
	{
		delete[] arr;
	}

private:
	T *arr; //wskaznik na pierwszy element
	int size;
	int elements;

public:
	int CurrentElements()const
	{
		return elements;
	}
	T& operator[](int i)
	{
		return arr[i];
	}
	const T& operator[](int i) const //overload operatora -- aby latwiej zwracac element -- operator musi zwracac referencje!
	{
		return arr[i];
	}
	void Pushback(const T& value) //dodawanie "na koniec"
	{
		CheckAndIncreaseCapacity();
		arr[elements] = value;
		++elements;
	}
	void Pop()
	{
		if (elements > 0)
		{
			--elements;
		}
	}
	void Insert(const T& value, int index) //dodanie do dowolnego "aktywnego" indeksu
	{
		CheckAndIncreaseCapacity();
		if (elements > 0 && elements > index)
		{
			for (int j = elements; j > index; --j)
			{
				arr[j] = arr[j - 1];
			}
		}
		++elements;
		arr[index] = value;
	}

private:
	void CheckAndIncreaseCapacity()
	{
		if (elements + 1 <= size) return;
		size *= 2;
		T*arr2 = new T[size];
		for (int i = 0; i < elements; i++)
		{
			arr2[i] = arr[i];
		}
		delete[] arr;
		arr = arr2;
	}
};

struct trieNode
{
	SimpleVector<char*>words; //vector wskaznikow na kolejne slowa -- bo moze miec kilka, moze vector vectorow char ?
	trieNode* children[8];
	trieNode()
	{
		for (int i = 0; i < CHILDREN_ARRAY_SIZE; i++)
		{
			children[i] = nullptr;
		}
	}
	~trieNode()
	{
		//usuniecie words
		for (int i = 0; i < words.CurrentElements(); ++i)
		{
			delete[] words[i];
		}
		//usuniecie nodow
		for (int i = 0; i < CHILDREN_ARRAY_SIZE; i++)
		{
			if (children[i] != nullptr)
			{
				delete children[i];
			}
		}
	}
};

int WordLength(const char *c) //liczy dlugosc slowa
{
	int i = 0;
	while (c[i] != '\0')
	{
		++i;
	}
	return i;
}

int LetterToIndex(char c) //funkcja zwracajaca indeks dla danej litery
{
	int letterCalc = c - 'a';
	if (letterCalc <= 2)
	{
		return 0;
	}
	if (letterCalc <= 5)
	{
		return 1;
	}
	if (letterCalc <= 8)
	{
		return 2;
	}
	if (letterCalc <= 11)
	{
		return 3;
	}
	if (letterCalc <= 14)
	{
		return 4;
	}
	if (letterCalc <= 18)
	{
		return 5;
	}
	if (letterCalc <= 21)
	{
		return 6;
	}
	if (letterCalc <= 25)
	{
		return 7;
	}
	return -1; //by stos nie plakal
}

int DigitCharToIndex(char c) //zwraca indeks dla cyfry jako char
{
	return c - '1' - 1; //dodatkowe -1int bo 2 w 0indeksie
}

int CompareStrings(const char* s1, const char* s2) //na podstawie strcmp, zwraca wieksze niz 0 gdy s1 jest wieksze, =0 gdy takie samo <0 gdy mniejsze
{
	while (*s1 && (*s1 == *s2))
		s1++, s2++;
	return *(const unsigned char*)s1 - *(const unsigned char*)s2;
}

void AddEquivWords(trieNode &node, char *word) //funkcja dodajaca slowa do wezla i sortujaca wskazniki na kolejne vectory charow
{
	int i, Comparator;
	for (i = 0; i < node.words.CurrentElements(); i++)
	{
		Comparator = CompareStrings(node.words[i], word);
		if (Comparator > 0)
		{
			break;
		}
	}
	char *newWord = new char[(WordLength(word) + 1)];
	for (int i = 0; i < WordLength(word) + 1; i++) //przepisujemy stara tablice do nowej
	{
		newWord[i] = word[i];
	}
	node.words.Insert(newWord, i);
}

trieNode *CheckChildLetter(char c, trieNode &node) //Funkcja sprawdza czy istnieje juz takie dziecko, O(1) - zamiast BinarySearch bo teraz jest statyczna
{
	int index = LetterToIndex(c);
	return node.children[index];
}

trieNode *CheckChildDigit(char digit, trieNode &node) //To samo co powyzej tylko konwersja dla char cyfr
{
	int index = DigitCharToIndex(digit); //tu tez sam return
	return node.children[index];
}

trieNode *AddChild(trieNode &node, char c) //funkcja dodajaca dziecko w odpowiednim indeksie
{
	trieNode *newChild = new trieNode;
	int index = LetterToIndex(c);
	node.children[index] = newChild;
	return newChild;
}

void AddWord(char *word, trieNode &root)//Funkcja dodajaca najpierw nowe objekty w odpowiednich indeksach i na koniec odpowiednie slowo
{
	trieNode *helper = &root; // wskazniki pomocnicze - do poruszania
	trieNode *helper2;
	for (int i = 0; i < WordLength(word); i++)
	{
		helper2 = CheckChildLetter(word[i], *helper);
		if (helper2 == nullptr)
		{
			helper = AddChild(*helper, word[i]);
		}
		else
		{
			helper = helper2;
		}
	}
	AddEquivWords(*helper, word);
}

void PrintAllEquivWords(const trieNode &node, bool *wasPrinted) //wypisuje wszystko odpowiadajace slowo poczawszy od podanego wezla, nastepnie jego dzieci
{
	if (node.words.CurrentElements() != 0) // to niepotrzebne
	{
		*wasPrinted = true;

		for (int i = 0; i < node.words.CurrentElements(); i++)
		{
			std::cout << node.words[i] << " ";
		}
	}
	for (int i = 0; i < CHILDREN_ARRAY_SIZE; i++)
	{
		if (node.children[i] != nullptr)
		{
			PrintAllEquivWords(*(node.children[i]), wasPrinted);
		}
	}
}

void PrintAll(const char *digit, trieNode &root)
{
	trieNode *helper = &root;
	trieNode *helper2;
	for (int i = 0; i < WordLength(digit); i++)
	{
		helper2 = CheckChildDigit(digit[i], *helper);
		if (helper2 == nullptr)
		{
			std::cout << "-" << std::endl;
			return;
		}
		else
		{
			helper = helper2;
		}
	}
	//helper zawiera node od ktorego wypisujemy wszystkie tlumaczenia
	bool wasPrinted = false; //zeby zapamietac jezeli wypisane
	PrintAllEquivWords(*helper, &wasPrinted);
	if (wasPrinted == false)
		std::cout << "-" << std::endl;
}

int main()
{
	trieNode T9root;
	char word[101];
	int command;
	std::cin >> command;
	while (command > 0)
	{
		--command;
		std::cin >> word;
		AddWord(word, T9root);
	}
	std::cin >> command;
	while (command > 0)
	{
		--command;
		std::cin >> word;
		PrintAll(word, T9root);
	}

	return 0;
}