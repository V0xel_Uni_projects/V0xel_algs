#include<iostream>

struct process
{
	int id;
	process *next;
};

struct List
{
	process *end;
	process *current;
};

void CreateProcess(process **end, int preid) //tworzy proces w jednej z list
{
	process *newitem = new process;
	newitem->id = preid;
	if (*end == nullptr) //jezeli pusta to na samego siebie
	{
		newitem->next = newitem;
		*end = newitem;
	}
	else
	{
		newitem->next = (*end)->next; //next nowego na next starego
		(*end)->next = newitem; //stary na nowy
		*end = newitem; //nowy staje sie koncowym
	}
}

void AssignProcess(process **current, process **end)//wypisuje na wyjscie numer aktualnego w danym priorytecie procesu
{
	if (*end == nullptr)
	{
		return;
	}
	if (*current == nullptr || *current == (*current)->next)
	{
		*current = (*end)->next;
		std::cout << (*current)->id << "\n";
		*current = (*current)->next;
	}
	else if (*current != (*current)->next)
	{
		std::cout << (*current)->id << "\n";
		*current = (*current)->next;
	}
}

process *SearchbyID(List table[], int preid, int &tableIndex)//funkcja szukajaca poprzednika szukanego elementu, tableindex by wiedziec w ktorym indeksie znalezlismy
{
	process *found;
	for (tableIndex = 0; tableIndex < 3; tableIndex++) //sprawdzamy kolejne indeksy w tablicy list
	{
		if (table[tableIndex].end != nullptr)//sprawdzamy tylko jezeli lista nie jest pusta
		{
			found = table[tableIndex].end;

			do
			{
				if (found->next->id == preid) //Jezeli nastepnik ma ID szukanego
				{
					return found;
				}

				found = found->next;
			} while (found != table[tableIndex].end);
		}
	}

	found = nullptr; //jezeli nie znaleziono
	return found;
}

void DeleteProcess(process *nextToDelete, int &tableIndex, List table[]) //usuwanie nastepnika
{
	if (nextToDelete == nullptr)
	{
		return;
	}
	if (nextToDelete->next == nextToDelete) //jezeli mielismy liste jednoelementowa
	{
		delete(nextToDelete);
		table[tableIndex].end = nullptr;
		table[tableIndex].current = nullptr;
		return;
	}

	process *helper = nextToDelete->next;

	if (helper == table[tableIndex].current) //sprawdzamy czy to co usuwamy nie jest currentem, jezeli tak to przesuwamy go
	{
		table[tableIndex].current = table[tableIndex].current->next;
	}
	if (nextToDelete->next == table[tableIndex].end) //jezeli kasujemy koncowy to przestawiamy go bo nasz poprzednik jest przedostatni
	{
		table[tableIndex].end = nextToDelete;
	}

	nextToDelete->next = nextToDelete->next->next; //helper->next ?
	delete helper;
}

void PrintList(process *end) //arg jako kopia
{
	if (end == nullptr)
	{
		return;
	}
	process *helper = end;
	do
	{
		helper = helper->next; //bo najpierw pomijamy koncowy
		std::cout << helper->id << " ";
	} while (helper != end); //dopoki nie wrocimy do koncowego
}

void PrintAll(List table[])
{
	std::cout << "1: "; PrintList(table[2].end); //tu n albo nie zalezy jak stos chce
	std::cout << "\n0: "; PrintList(table[1].end);
	std::cout << "\n-1: "; PrintList(table[0].end);
	std::cout << "\n";
}

void Initialize(List table[])
{
	for (int i = 0; i < 3; i++)
	{
		table[i].end = nullptr;
		table[i].current = nullptr;
	}
}

void CheckPriority(List table[], int &priority) //Funkcja sprawdza liste z najwyzszym priorytetem ktora nie jest pusta
{
	for (int i = 2; i >= 0; i--)
	{
		if (table[i].end != nullptr)
		{
			priority = i;
			return;
		}
	}
	std::cout << "idle\n";
}

int main()
{
	int preid = 0, priority = 0, tableIndex = 0;
	char input;
	List table[3];
	Initialize(table);
	while (std::cin >> input)
	{
		switch (input)
		{
		case 'n':
			CheckPriority(table, priority);
			AssignProcess(&table[priority].current, &table[priority].end);
			break;
		case 'c':
			std::cin >> preid;
			std::cin >> priority;
			priority += 1; //bo priority indeksem, mozna by jedna zmienna tak naprawde razem z tym tableIndex, rozrozniamy ze tableIndex jest z szukania po ID
			CreateProcess(&table[priority].end, preid);
			break;
		case 't':
			std::cin >> preid;
			DeleteProcess(SearchbyID(table, preid, tableIndex), tableIndex, table);
			break;
		case 'p':
			std::cin >> preid;
			std::cin >> priority;
			priority += 1;
			DeleteProcess(SearchbyID(table, preid, tableIndex), tableIndex, table);
			CreateProcess(&table[priority].end, preid);
			break;
		case 'l':
			PrintAll(table);
			break;
		default:
			break;
		}
	}

	return 0;
}