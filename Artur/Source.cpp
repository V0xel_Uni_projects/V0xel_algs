#include<iostream>
const int MAX_INT_SIZE = 2147483647;
enum VillageType
{
	normal, bushVendor, blackKnight, niKnights, arthurCastle, grail
};

template <class T>
class SimpleVector
{
public:
	SimpleVector(int s) : size(s), elements(s)
	{
		arr = new T[size];
	}
	SimpleVector() : size(2), elements(0)
	{
		arr = new T[size];
	}
	~SimpleVector()
	{
		delete[] arr;
	}

private:
	T *arr;
	int size;
	int elements;

public:
	int CurrentElements()const
	{
		return elements;
	}
	T& operator[](int i)
	{
		return arr[i];
	}
	const T& operator[](int i) const
	{
		return arr[i];
	}
	void Pushback(const T& value)
	{
		CheckAndIncreaseCapacity();
		arr[elements] = value;
		++elements;
	}
	void Pop()
	{
		if (elements > 0)
		{
			--elements;
		}
	}
	void Insert(const T& value, int index)
	{
		CheckAndIncreaseCapacity();
		if (elements > 0 && elements > index)
		{
			for (int j = elements; j > index; --j)
			{
				arr[j] = arr[j - 1];
			}
		}
		++elements;
		arr[index] = value;
	}

private:
	void CheckAndIncreaseCapacity()
	{
		if (elements + 1 <= size) return;
		size *= 2;
		T*arr2 = new T[size];
		for (int i = 0; i < elements; i++)
		{
			arr2[i] = arr[i];
		}
		delete[] arr;
		arr = arr2;
	}
};

class Village
{
public:
	Village() : distFromCastle(MAX_INT_SIZE), distFromBush(MAX_INT_SIZE), head(nullptr), prevVillageCastle(nullptr), prevVillageBush(nullptr),
		closedCastle(false), closedNi(false), inQueueCastle(false), inQueueNi(false)
	{
	}

public:
	VillageType type;

	int distFromCastle;
	int distFromBush;
	int villageNumber;

	Village *prevVillageCastle;
	Village *prevVillageBush;

	bool closedCastle;
	bool closedNi;
	bool inQueueCastle;
	bool inQueueNi;

	static bool typeToCompare;

public:
	struct List
	{
		List *next;
		int distanceToNeighbour;
		Village *neighbour;
	};

	List *head;

public:
	void AddToList(int distance, Village *neighbour)
	{
		List *newNeighbour = new List;
		newNeighbour->distanceToNeighbour = distance;
		newNeighbour->neighbour = neighbour;
		newNeighbour->next = head;
		head = newNeighbour;
	}

	//--------------------------------------Overloady operatorow-----------------------------------// Aby kolejka dzialala dla obu kluczy

	bool operator<(const Village &object)
	{
		if (typeToCompare)
		{
			return this->distFromCastle < object.distFromCastle;
		}
		else
		{
			return this->distFromBush < object.distFromBush;
		}
	}

	bool operator>(const Village &object)
	{
		if (typeToCompare)
		{
			return this->distFromCastle > object.distFromCastle;
		}
		else
		{
			return this->distFromBush > object.distFromBush;
		}
	}

	bool operator<(const int& value)
	{
		if (typeToCompare)
		{
			return this->distFromCastle < value;
		}
		else
		{
			return this->distFromBush < value;
		}
	}

	bool operator>(const int& value)
	{
		if (typeToCompare)
		{
			return this->distFromCastle > value;
		}
		else
		{
			return this->distFromBush > value;
		}
	}

	int& operator=(const int& key)
	{
		if (typeToCompare)
		{
			return this->distFromCastle = key;
		}
		else
		{
			return this->distFromBush = key;
		}
	}

	//-------------------------------------------------------------------------------------------------------//
private:
	void ClearList()
	{
		while (head!=nullptr)
		{
			List *helper;
			helper = head;
			head = head->next;
			delete helper;
		}
	}
	public:
		~Village()
		{
			ClearList();
		}
};

class MinHeapQueue //Indeksowana kolejka priorytetowa, potrzebna zeby logn w decrease key
{
public:
	MinHeapQueue(int s) : Indexing(s) {}
private:
	SimpleVector<Village*>heap;
	SimpleVector<int>Indexing; //vectoro-tablica przechowujaca indexy heap, index Indexing jest ID wioski, zeby w logn robic Decrease na dowolnym, -1 gdy brak

public:
	void Insert(Village *object)
	{
		int parentPos, currentIndex = heap.CurrentElements();
		Indexing[object->villageNumber] = currentIndex; //zapisanie w jakim indeksie wioska o danym ID znajduje sie w heap
		heap.Pushback(object);
		if (currentIndex == 0) return;
		parentPos = (currentIndex - 1) / 2;
		while (currentIndex > 0 && *heap[parentPos] > *object) //porownujemy obiekty -> ich klucze
		{
			swap(currentIndex, parentPos);
			currentIndex = parentPos;
			parentPos = (currentIndex - 1) / 2;
		}
		heap[currentIndex] = object;
	}

	Village* ExtractMin()
	{
		if (heap.CurrentElements() == 0) return nullptr;
		Village *extracted;
		extracted = heap[0];
		swap(0, heap.CurrentElements() - 1);
		heap.Pop();
		MinHeapify(0);
		Indexing[extracted->villageNumber] = -1; //gdy brak to -1 !!!
		return extracted;
	}

	void DecreaseKey(Village *object, int key)
	{
		int index = Indexing[object->villageNumber]; //znajdujemy Index O(1)
		if (*heap[index] < key) return; //podany klucz jest wiekszy niz zaktualny istniejacy
		*heap[index] = key;
		if (index == 0) return;
		int parentPos = (index - 1) / 2;
		while (index > 0 && *heap[parentPos] > key) //naprawa w gore - O(logn)
		{
			swap(index, parentPos);
			index = parentPos;
			parentPos = (index - 1) / 2;
		}
		*heap[index] = key;
	}

	bool notEmpty()
	{
		if (heap.CurrentElements() == 0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

private:
	void MinHeapify(int index) //index startowy, naprawa w dol
	{
		while (index <= heap.CurrentElements())
		{
			int left = 2 * index + 1, right = 2 * index + 2, smallest;
			Village *helper;

			if (left < heap.CurrentElements() && *heap[left] < *heap[index])
			{
				smallest = left;
			}
			else
			{
				smallest = index;
			}
			if (right < heap.CurrentElements() && *heap[right] < *heap[smallest])
			{
				smallest = right;
			}
			if (smallest != index)
			{
				swap(index, smallest);
				index = smallest;
			}
			else
			{
				break;
			}
		}
	}
	void swap(int indexA, int indexB)
	{
		Village *helper;
		helper = heap[indexA];
		heap[indexA] = heap[indexB];
		heap[indexB] = helper;
		Indexing[heap[indexA]->villageNumber] = indexA; //aktualizacja w tablicy indeksow
		Indexing[heap[indexB]->villageNumber] = indexB;
	}
};

bool Village::typeToCompare = true;

void DijkstraBush(Village *source, int blackTime, int totalVillages)
{
	Village::typeToCompare = false; //zmieniamy klucz wzgl ktorego porownania dla kolejki
	Village *extractor;
	extractor->typeToCompare = false;
	source->distFromBush = 0;
	MinHeapQueue Queue(totalVillages);
	Queue.Insert(source);
	source->inQueueNi = true;
	while (Queue.notEmpty())
	{
		extractor = Queue.ExtractMin();
		extractor->closedNi = true;
		extractor->inQueueNi = false;

		if (extractor->type == niKnights) //jezeli zdjeci Ni
		{
			return;
		}
		Village::List *temp = extractor->head;
		while (temp != nullptr)
		{
			int altPath;
			if (temp->neighbour->type == blackKnight)
			{
				altPath = extractor->distFromBush + temp->distanceToNeighbour + blackTime;
			}
			else
			{
				altPath = extractor->distFromBush + temp->distanceToNeighbour;
			}

			if (temp->neighbour->closedNi == false && temp->neighbour->inQueueNi == false) //jest otwarty i nie ma go w kolejce
			{
				temp->neighbour->distFromBush = altPath;
				temp->neighbour->prevVillageBush = extractor; //zapamietanie wioski z ktorej sie dotarlo
				temp->neighbour->inQueueNi = true; //bo bedzie dodany do kolejki
				Queue.Insert(temp->neighbour);
			}
			else if (temp->neighbour->inQueueNi == true && temp->neighbour->distFromBush > altPath) //jezeli w kolejce i aktualna waga wieksza niz nowa
			{
				temp->neighbour->prevVillageBush = extractor;
				Queue.DecreaseKey(temp->neighbour, altPath);

			}
			temp = temp->next;
		}
	}
}

void DijkstraCastle(int source, int blackTime, SimpleVector<Village>&allVillages, int bushIndex, int totalVillages)
{
	Village *extractor; //pomocniczy wskaznik do extrakcji min
	allVillages[source].distFromCastle = 0; //ustawienie wagi zrodla
	MinHeapQueue Queue(totalVillages);
	Queue.Insert(&allVillages[source]); //dodanie zrodla do kolejki
	allVillages[source].inQueueCastle = true;
	while (Queue.notEmpty())
	{
		extractor = Queue.ExtractMin();
		extractor->closedCastle = true;
		extractor->inQueueCastle = false;

		if (extractor->type == niKnights) //jezeli zdjeci rycerze Ni to zalaczamy dijkstre od krzaka -- przy wypisywaniu tak wyszlo ze zamiast od Ni
		{
			DijkstraBush(&allVillages[bushIndex], blackTime, totalVillages);
			Village::typeToCompare = true; //bo w Ni bedzie przestawione na false
		}
		if (extractor->type == grail)
		{
			return;
		}
		Village::List *temp = extractor->head;
		while (temp != nullptr) //przeszukanie sasiadow
		{
			int altPath;

			if (extractor->type == niKnights) //jezeli przeszukujemy sasiadow od Ni
			{
				if (temp->neighbour->type == blackKnight)
				{
					altPath = extractor->distFromCastle + temp->distanceToNeighbour + blackTime + (extractor->distFromBush * 2); //dist from Ni bo kopiujemy odleglosc z krzaka 
				}
				else
				{
					altPath = extractor->distFromCastle + temp->distanceToNeighbour + (extractor->distFromBush * 2);
				}
			}
			else
			{
				if (temp->neighbour->type == blackKnight)
				{
					altPath = extractor->distFromCastle + temp->distanceToNeighbour + blackTime;
				}
				else
				{
					altPath = extractor->distFromCastle + temp->distanceToNeighbour;
				}
			}
			if (temp->neighbour->closedCastle == false && temp->neighbour->inQueueCastle == false) //jest otwarty i nie ma go w kolejce
			{
				temp->neighbour->distFromCastle = altPath;
				temp->neighbour->prevVillageCastle = extractor; // zapamietanie wioski z ktorej sie dotarlo
				temp->neighbour->inQueueCastle = true; //bo bedzie dodany do kolejki
				Queue.Insert(temp->neighbour); //dodanie
			}
			else if (temp->neighbour->inQueueCastle == true && temp->neighbour->distFromCastle > altPath) //jezeli w kolejce i aktualna waga wieksza niz nowa
			{
				temp->neighbour->prevVillageCastle = extractor;
				Queue.DecreaseKey(temp->neighbour, altPath); //zmniejszenie klucza
			}
			temp = temp->next; //przjescie na kolejnego sasiada na liscie
		}
	}
}

void PrintNi(Village &node) //dwa wypisania by wypisac powrot
{
	if (node.prevVillageBush != nullptr)
	{
		std::cout << node.villageNumber << " ";
		PrintNi(*node.prevVillageBush);
	}
	std::cout << node.villageNumber << " ";
}

void PrintPath(Village *node) //wypisanie calej sciezki
{
	if (node->type == niKnights)
	{
		PrintPath(node->prevVillageCastle); //zeby sciezka krzaka do Ni i z powrotem  byla  "w srodku"
		PrintNi(*node); //wypisanie sciezki ni-krzak-ni, ni jest koncowym w DijkstraBush
		return;
	}
	else if (node->prevVillageCastle != nullptr)
	{
		PrintPath(node->prevVillageCastle);
	}
	std::cout << node->villageNumber << " ";
}

VillageType Convert(int toConvert)
{
	switch (toConvert)
	{
	case 0:
		return normal;
		break;
	case 1:
		return bushVendor;
		break;
	case 2:
		return blackKnight;
		break;
	case 3:
		return niKnights;
		break;
	case 4:
		return arthurCastle;
		break;
	case 5:
		return grail;
		break;
	default:
		return normal;
		break;
	}
}

int main()
{
	int villages, villagesCopy, blackKnightTime, typeToConvert, arthurIndex, grailIndex, bushIndex = -1, neighboursAmount; //villages copy potrzebne do konstruktora heap
	while (std::cin >> villages)
	{
		villagesCopy = villages;
		std::cin >> blackKnightTime;
		SimpleVector<Village>allVillages(villages); //vector przechowujacy osady
		while (villages > 0) // wejscie
		{
			int index = allVillages.CurrentElements() - villages;
			int destination, length;

			std::cin >> typeToConvert;
			allVillages[index].type = Convert(typeToConvert); //dodawanie typu
			allVillages[index].villageNumber = index; //ID wioski

			if (allVillages[index].type == arthurCastle)
			{
				arthurIndex = index;
			}
			if (allVillages[index].type == grail)
			{
				grailIndex = index;
			}
			if (allVillages[index].type == bushVendor)
			{
				bushIndex = index;
			}
			for (std::cin >> neighboursAmount; neighboursAmount > 0; neighboursAmount--) //dodawanie sasiadow
			{
				std::cin >> destination;
				std::cin >> length;
				allVillages[index].AddToList(length, &allVillages[destination]);
			}
			--villages;
		}

		DijkstraCastle(arthurIndex, blackKnightTime, allVillages, bushIndex, villagesCopy);
		PrintPath(&allVillages[grailIndex]);
	}

	return 0;
}