#include<iostream>
struct element
{
	int first;
	int second;
};

element *CreateDynamicArray(int size) //zwraca wlasciciela, tworzyc o jeden wieksza? -> indeksujemy od 1 -> 0 sie marnuje
{
	element *arr = new element[(size + 1)];
	return arr;
}

int *CreateDynamicCountingArray(int max)
{
	int *carr = new int[max];
	return carr;
}

void CleanArray(element *arr)
{
	delete[] arr;
}

void CleanArray(int *arr)
{
	delete[] arr;
}

void ZeroCountingArray(int *arr, int size)
{
	for (int i = 0; i < size; i++)//bo 0 zawsze bedzie nasza min wartoscia
	{
		arr[i] = 0;
	}
}

void SaveToArray(element *arr, int size)
{
	int helper;
	for (int i = 1; i < (size + 1); i++) //bedziemy zostawiac pierwszy element unitialized->legit?
	{
		std::cin >> arr[i].first;
		std::cin >> arr[i].second;
	}
}

int main()
{
	int size = 0, task;
	element *mainPreArray;
	element *mainPostArray;
	int *countingArray;

	while (std::cin >> size)
	{
		mainPreArray = CreateDynamicArray(size);
		mainPostArray = CreateDynamicArray(size);
		countingArray = CreateDynamicCountingArray(size);
		SaveToArray(mainPreArray, size);
		ZeroCountingArray(countingArray, size);
		for (int i = 1; i < (size + 1); i++)//petla przeglada klucze t.nieposortow i zaleznie od wartosci +1 w indeksie t.sortujacej wielkosc+1 bo wzgledem ilosci elementow
		{
			countingArray[mainPreArray[i].first]++;
		}
		for (int i = 1; i < size; i++)//i=1 bo zaczynamy od drugiego indeksu->ile wartosci <= indeksowi
		{
			countingArray[i] += countingArray[i - 1];
		}
		for (int i = size; i >= 1; i--) //przepisujmey elementy z t.poczatkowej do koncowej i -1 w counting
		{
			mainPostArray[countingArray[mainPreArray[i].first]] = mainPreArray[i];
			countingArray[mainPreArray[i].first]--;
		}

		std::cin >> task;

		switch (task)
		{
		case 0:
			for (int i = 1; i < (size + 1); i++)
			{
				std::cout << mainPostArray[i].first << "\n";
			}
			break;
		case 1:
			for (int i = 1; i < (size + 1); i++)
			{
				std::cout << mainPostArray[i].first << ",";
				std::cout << mainPostArray[i].second << "\n";
			}
			break;
		default:
			break;
		}
		CleanArray(mainPreArray);
		CleanArray(mainPostArray);
		CleanArray(countingArray);
	}

	return 0;
}